use ggez::conf::FullscreenType;
use ggez::event::{KeyCode, KeyMods, MouseButton};
use ggez::graphics::{DrawParam, Font, Image, PxScale, Text};
use ggez::{event, graphics, timer, Context, GameResult};
use rand::Rng;

const SCREEN_SIZE: (f32, f32) = (1920 as f32, 1080 as f32);

const DESIRED_FPS: u32 = 60;

//TODO
//first click must be safe
//so bomb gen must be done after the first click
//also should merge all matrices into one
//separar el self.new()
//cuando haces click por primera vez rellena las bombas con un area de proteccion 3x3 alrededor del click y luego pinta
fn draw_grid(
    ctx: &mut Context,
    posx: i32,
    posy: i32,
    width: i32,
    length: i32,
    color: graphics::Color,
) -> graphics::Mesh {
    let line = graphics::Mesh::new_rectangle(
        ctx,
        graphics::DrawMode::fill(),
        graphics::Rect::new_i32(
            posx,   //posx
            posy,   //posy
            width,  //width
            length, //length
        ),
        color,
    );
    match line {
        Ok(lin) => return lin,
        Err(_) => panic!(),
    }
}



#[derive(Debug)]
struct GameState {
    gameover: bool,
    clicks : Vec<i32>,
    bombs: [[i32; 30]; 16],
    open: [[i32; 30]; 16],
}
: Vec<i32>
impl GameState {
    /// Our new function will set up the initial state of our game.
    pub fn new() -> Self {
        let mut clickIndex = vec![0,0];
        let mut bomb_matrix = [[0; 30]; 16];
        let opened_squares = [[0; 30]; 16];
        let _cx = 240 - 75;
        let _cy = 156 - 75 / 2;
        for _i in 0..99 {
            let mut x = rand::thread_rng().gen_range(0..30);
            let mut y = rand::thread_rng().gen_range(0..16);
            while bomb_matrix[y][x] != 0 {
                x = rand::thread_rng().gen_range(0..30);
                y = rand::thread_rng().gen_range(0..16);
            }
            bomb_matrix[y][x] = 1;
        }
        GameState {
            clicks : clickIndex,
            gameover: false,
            bombs: bomb_matrix,
            open: opened_squares,

        }
    }

    //complicado sistema implementado por mecanica de poleas alternas
    fn get_neighbours(&mut self, posx: i32, posy: i32) -> Vec<Vec<i32>> {
        let mut neighbours = vec![];

        match posx {
            0 => {
                match posy {
                    0 => {
                        let v1 = vec![1, 0];
                        let v2 = vec![0, 1];
                        let v3 = vec![1, 1];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                    }
                    15 => {
                        let v1 = vec![0, 14];
                        let v2 = vec![1, 14];
                        let v3 = vec![1, 15];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                    }
                    _ => {
                        let v1 = vec![0, posy - 1];
                        let v2 = vec![0, posy + 1];
                        let v3 = vec![1, posy - 1];
                        let v4 = vec![1, posy + 1];
                        let v5 = vec![1, posy];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                        neighbours.push(v4);
                        neighbours.push(v5);
                    }
                }
                neighbours
            }
            29 => {
                match posy {
                    0 => {
                        let v1 = vec![28, 0];
                        let v2 = vec![28, 1];
                        let v3 = vec![29, 1];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                    }
                    15 => {
                        let v1 = vec![28, 15];
                        let v2 = vec![28, 14];
                        let v3 = vec![29, 14];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                    }
                    _ => {
                        let v1 = vec![29, posy - 1];
                        let v2 = vec![29, posy + 1];
                        let v3 = vec![28, posy - 1];
                        let v4 = vec![28, posy];
                        let v5 = vec![28, posy + 1];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                        neighbours.push(v4);
                        neighbours.push(v5);
                    }
                }
                neighbours
            }
            _ => {
                match posy {
                    0 => {
                        let v1 = vec![posx - 1, 0];
                        let v2 = vec![posx + 1, 0];
                        let v3 = vec![posx - 1, 1];
                        let v4 = vec![posx, 1];
                        let v5 = vec![posx + 1, 1];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                        neighbours.push(v4);
                        neighbours.push(v5);
                    }
                    15 => {
                        let v1 = vec![posx - 1, 15];
                        let v2 = vec![posx + 1, 15];
                        let v3 = vec![posx - 1, 14];
                        let v4 = vec![posx, 14];
                        let v5 = vec![posx + 1, 14];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                        neighbours.push(v4);
                        neighbours.push(v5);
                    }
                    _ => {
                        let v1 = vec![posx - 1, posy];
                        let v2 = vec![posx + 1, posy];
                        let v3 = vec![posx - 1, posy - 1];
                        let v4 = vec![posx, posy - 1];
                        let v5 = vec![posx + 1, posy - 1];
                        let v6 = vec![posx - 1, posy + 1];
                        let v7 = vec![posx, posy + 1];
                        let v8 = vec![posx + 1, posy + 1];
                        neighbours.push(v1);
                        neighbours.push(v2);
                        neighbours.push(v3);
                        neighbours.push(v4);
                        neighbours.push(v5);
                        neighbours.push(v6);
                        neighbours.push(v7);
                        neighbours.push(v8);
                    }
                }
                neighbours
            }
        }
    }

    fn floodfill_reveal(&mut self, x: i32, y: i32) {
        let mut index = 0;
        let mut neighbours = self.get_neighbours(x, y);
        //si el numero de bombas alrededor es superior a 0 nos piramos
        for i in 0..neighbours.len() {
            if self.bombs[neighbours[i][1] as usize][neighbours[i][0] as usize] == 1 {
                index += 1;
                break;
            }
        }
        if index == 0 && self.bombs[y as usize][x as usize] == 0 {
            while !neighbours.is_empty() {
                if self.open[neighbours[neighbours.len() - 1][1] as usize]
                    [neighbours[neighbours.len() - 1][0] as usize]
                    == 0
                {
                    self.open[neighbours[neighbours.len() - 1][1] as usize]
                        [neighbours[neighbours.len() - 1][0] as usize] = 1;
                    self.floodfill_reveal(
                        neighbours[neighbours.len() - 1][0],
                        neighbours[neighbours.len() - 1][1],
                    );
                }
                neighbours.pop();
            }
        }
    }

    fn reveal_bombs(&mut self) {
        for i in 0..30 {
            for j in 0..16 {
                if self.bombs[j][i] == 1 && self.open[j][i] == 0 {
                    self.open[j][i] = 3;
                    self.gameover = true;
                }
            }
        }
    }
}

impl event::EventHandler<ggez::GameError> for GameState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        while timer::check_update_time(ctx, DESIRED_FPS) {
            if !self.gameover {}
        }
        Ok(())
    }
    
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        let flag = Image::new(ctx, "/flag.png");
        let fpng = flag.expect("no flag.png found bro");
        let mine = Image::new(ctx, "/mine.png");
        let mpng = mine.expect("no mine.png found bro");
        let cross = Image::new(ctx, "/cross.png");
        let cpng = cross.expect("no cross.png found bro");

        graphics::clear(ctx, [0.25, 0.25, 0.25, 1.0].into());
        let scale = PxScale::from(55.0);
        let startx = 240 - 75;
        let starty = 156 - 75 / 2;
        //draw grid
        for i in 0..30 {
            for j in 0..16 {
                let color;
                if self.open[j][i] == 0 {
                    color = [0.75, 0.75, 0.75, 1.0].into();
                } else {
                    color = [0.6, 0.6, 0.6, 1.0].into();
                }

                let draw = draw_grid(
                    ctx,
                    startx + i as i32 * 48 + 5 * i as i32,
                    starty + j as i32 * 48 + 5 * j as i32,
                    48,
                    48,
                    color,
                );
                graphics::draw(ctx, &draw, DrawParam::default())?;
            }
        }
        //draw mines
        for i in 0..30 {
            for j in 0..16 {
                if self.bombs[j][i] == 1 && self.open[j][i] == 1 {
                    graphics::draw(
                        ctx,
                        &mpng,
                        DrawParam::default().dest(ggez::mint::Point2 {
                            x: (startx + i as i32 * 48 + 5 * i as i32 + 7) as f32,
                            y: (starty + j as i32 * 48 + 5 * j as i32 + 5) as f32,
                        }),
                    )?;
                    graphics::draw(
                        ctx,
                        &cpng,
                        DrawParam::default().dest(ggez::mint::Point2 {
                            x: (startx + i as i32 * 48 + 5 * i as i32 + 7) as f32,
                            y: (starty + j as i32 * 48 + 5 * j as i32 + 5) as f32,
                        }),
                    )?;
                    self.reveal_bombs();
                } else if self.open[j][i] == 2 {
                    graphics::draw(
                        ctx,
                        &fpng,
                        DrawParam::default().dest(ggez::mint::Point2 {
                            x: (startx + i as i32 * 48 + 5 * i as i32 + 7) as f32,
                            y: (starty + j as i32 * 48 + 5 * j as i32 + 5) as f32,
                        }),
                    )?;
                } else if self.open[j][i] == 3 {
                    graphics::draw(
                        ctx,
                        &mpng,
                        DrawParam::default().dest(ggez::mint::Point2 {
                            x: (startx + i as i32 * 48 + 5 * i as i32 + 7) as f32,
                            y: (starty + j as i32 * 48 + 5 * j as i32 + 5) as f32,
                        }),
                    )?;
                }
            }
        }
        //draw bomb counter
        for i in 0..30 {
            for j in 0..16 {
                if self.open[j][i] == 1 && self.bombs[j][i] == 0 {
                    //ans es el array de vecinos, es decir array de arrays
                    let ans = self.get_neighbours(i as i32, j as i32);
                    let mut counter = 0;
                    for k in 0..ans.len() {
                        if self.bombs[ans[k][1] as usize][ans[k][0] as usize] == 1 {
                            counter += 1;
                        }
                    }
                    let string = format!("{}", counter);
                    let mut text = Text::new(string);
                    text.set_font(Font::default(), scale);
                    if counter != 0 {
                        let color;
                        match counter {
                            1 => {
                                color = [0.0, 0.0, 0.75, 1.0].into();
                            }
                            2 => {
                                color = [0.0, 1.0, 0.0, 1.0].into();
                            }
                            3 => {
                                color = [1.0, 0.0, 0.0, 1.0].into();
                            }
                            4 => {
                                color = [0.0, 0.0, 1.0, 1.0].into();
                            }
                            5 => {
                                color = [0.75, 0.0, 0.0, 1.0].into();
                            }
                            6 => {
                                color = [0.0, 0.75, 0.75, 1.0].into();
                            }
                            7 => {
                                color = [0.0, 1.0, 1.0, 1.0].into();
                            }
                            8 => {
                                color = [0.0, 1.0, 1.0, 1.0].into();
                            }
                            _ => {
                                color = [0.0, 1.0, 1.0, 1.0].into();
                            }
                        }
                        graphics::draw(
                            ctx,
                            &text,
                            DrawParam::default()
                                .dest(ggez::mint::Point2 {
                                    x: (startx + i as i32 * 48 + 5 * i as i32 + 9) as f32,
                                    y: (starty + j as i32 * 48 + 5 * j as i32) as f32,
                                })
                                .color(color),
                        )?;
                    }
                }
            }
        }
        if self.gameover == true {
            let string = format!("It's gameover man!\nPress R to play again :D");
            let mut text = Text::new(string);
            text.set_font(Font::default(), scale);
            graphics::draw(
                ctx,
                &text,
                DrawParam::default()
                    .dest(ggez::mint::Point2 {
                        x: (1920 / 2 - 400) as f32,
                        y: (1080 / 2 - 50) as f32,
                    })
                    .color([0.0, 1.0, 1.0, 1.0].into()),
            )?;
        }
        graphics::present(ctx)?;
        ggez::timer::yield_now();
        Ok(())
    }

    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        keycode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) {
        if keycode == KeyCode::R {
            *self = GameState::new();
        }
    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        _button: MouseButton,
        _x: f32,
        _y: f32,
    ) {
        if self.gameover != true {
            let startx = (240 - 75) as f32 as f32;
            let starty = (156 - 75 / 2) as f32;
            let mut posx = _x - startx;
            if posx != 0 as f32 {
                posx = posx / 53 as f32;
            }
            let mut posy = _y - starty;
            if posy != 0 as f32 {
                posy = posy / 53 as f32;
            }
            match _button {
                MouseButton::Left => {
                    if posy > 0 as f32 && posy < 16 as f32 && posx > 0 as f32 && posx < 30 as f32 {
                        if self.open[posy as usize][posx as usize] == 0 {
                            self.open[posy as usize][posx as usize] = 1;
                            self.floodfill_reveal(posx as i32, posy as i32);
                        }
                    }
                }
                MouseButton::Right => {
                    if posy > 0 as f32 && posy < 16 as f32 && posx > 0 as f32 && posx < 30 as f32 {
                        match self.open[posy as usize][posx as usize] {
                            0 => {
                                self.open[posy as usize][posx as usize] = 2;
                            }
                            2 => {
                                self.open[posy as usize][posx as usize] = 0;
                            }
                            _ => {}
                        }
                    }
                }
                _ => {}
            }
        }
    }
}

fn main() -> GameResult {
    let (ctx, events_loop) = ggez::ContextBuilder::new("minesweeper", "el tito alex")
        .window_setup(ggez::conf::WindowSetup::default().title("minesweeper!"))
        .window_mode(
            ggez::conf::WindowMode::default()
                .dimensions(SCREEN_SIZE.0, SCREEN_SIZE.1)
                .borderless(true)
                .fullscreen_type(FullscreenType::True),
        )
        .build()?;
    let state = GameState::new();
    event::run(ctx, events_loop, state)
}
